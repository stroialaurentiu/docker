#!/usr/bin/env bash

case "$1" in
    "watch")
    echo "Running the container in interactive bash mode"
    docker run -ti -v `pwd`/dianabotezan:/assets dockerubuntu_assets gulp watch
    ;;

    *)
    echo -e "\n\tScript usage: [watch] \n"
    exit 1 # Command to come out of the program with status 1
    ;;
esac
