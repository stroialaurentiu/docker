FROM php:7-apache

MAINTAINER Stroia Laurentiu

EXPOSE 80

RUN apt-get update && apt-get -y install zip nano mysql-client && \
	export TERM=xterm && \
	(echo "192.168.99.100 dbhost" >> /etc/hosts) && \
	(echo "127.0.0.1 dianabotezan.ro www.dianabotezan.ro" >> /etc/hosts) && \
	mkdir /etc/apache2/sites-enabled/extra && echo "IncludeOptional sites-enabled/extra/*.conf" >> /etc/apache2/apache2.conf && \
	(curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer)

WORKDIR /var/www
