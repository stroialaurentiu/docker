# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
This repo help to start a LAMP on your project
* Version
PHP 7
MySQL 5.7
Apache 2

### How do I get set up? ###

* Summary of set up
First need to have installed [docker](https://www.docker.com/)
* Configuration
* 1.  Clone repo to your project
2.  run `/run.sh bash`
3.  At least start MySql && Apache: ` servivce mysql restart && service apache2 start`
* Dependencies
* 1.  [docker](https://www.docker.com/)
* Database configuration
* 1.  for mysql exist user 'root' without password ('')
* How to run tests
* 1.  http://192.168.99.100/ (default IP for VM, make sure all containers are closed )

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact